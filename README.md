# Wordpress integration for Dz Framework

## Installation

Add these lines to composer.json file:

```shell
"require": {
    ...
    "dezero/wordpress": "dev-master"
    ...
},
"repositories":[
    ...
    {
        "type": "vcs",
        "url" : "git@bitbucket.org:dezero/wordpress.git"
    }
    ...
]
```

## Configuration

1) Define the module in the configuration file `/app/config/common/modules.php`
```shell

    // Wordpress module
    'wordpress' => [
        'class' => '\dzlab\wordpress\Module',
        // 'class' => '\wordpress\Module',
    ],
```


2) Add a new alias path in `/app/config/common/aliases.php`
```shell
    'dzlab.wordpress'  => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'dezero' . DIRECTORY_SEPARATOR . 'wordpress',
```


3) Set new component in `/app/config/common/components.php`
```shell
    // Wordpress contrib component
    'wordpressManager' => [
        'class' => '\dzlab\wordpress\components\WordpressManager'
    ],
```


4) Register a log file for Wordpress module in `/app/config/components/logs.php`
```shell
    // Logs for Wordpress module
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'wp_sync.log',
        'categories' => 'wp_sync',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'wp_login.log',
        'categories' => 'wp_login',
    ],
```


5) Configurate database via environment variables from the `.env` file.
```shell
# Wordpress - Website URL
WP_URL="http://site.wordpress"

# Wordpress - The database driver that will used ('mysql' or 'pgsql')
WP_DRIVER="mysql"

# Wordpress - The database server name or IP address (usually this is 'localhost' or '127.0.0.1')
WP_SERVER="localhost"

# Wordpress - The database username to connect with
WP_USER="REPLACE_ME"

# Wordpress - The database password to connect with
WP_PASSWORD="REPLACE_ME"

# Wordpress - The name of the database to select
WP_DATABASE="REPLACE_ME"

# Wordpress - The port to connect to the database with. Will default to 5432 for PostgreSQL and 3306 for MySQL.
WP_PORT="3306"

# Wordpress - The Wordpress database prefix
WP_PREFIX="REPLACE_ME"
```

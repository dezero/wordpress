<?php
/**
 * Migration class m210104_104709_wordpress_user_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210104_104709_wordpress_user_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "wordpress_user" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('wordpress_user', true);

        $this->createTable('wordpress_user', [
            'wp_user_id' => $this->integer()->unsigned()->notNull(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'sync_code' => $this->string(255),
            'wp_user_login' => $this->string(60)->notNull(),
            'wp_user_nicename' => $this->string(50)->notNull(),
            'wp_user_email' => $this->string(100)->notNull(),
            'wp_user_registered' => $this->date()->notNull(),
            'wp_user_status' => $this->integer()->notNull()->defaultValue(0),
            'wp_display_name' => $this->string(250)->notNull(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull()
        ]);
    
        // Primary key (alternative method)
        $this->addPrimaryKey(null, 'wordpress_user', 'wp_user_id');

        // Create indexes
        $this->createIndex(null, 'wordpress_user', ['user_id'], false);
        $this->createIndex(null, 'wordpress_user', ['wp_user_login'], false);
        $this->createIndex(null, 'wordpress_user', ['wp_user_email'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'wordpress_user', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'wordpress_user', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'wordpress_user', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('wordpress_user');
		return false;
	}
}


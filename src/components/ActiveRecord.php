<?php
/**
 * ActiveRecord class file for Wordpress database connection
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dzlab\wordpress\components;

use dz\db\ActiveRecord as CoreActiveRecord;
use dz\db\AssetFileTrait;
use dz\db\DbCriteria;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\components\WordpressTrait;
use Yii;

/**
 * ActiveRecord is the base class for the generated AR (base) models.
 */
abstract class ActiveRecord extends CoreActiveRecord
{
    /**
     * Trait class with methods for working with Wordpress data
     */
    use WordpressTrait;


    /**
     * Wordpress prefix used for database tables
     */
    private $_wordpress_prefix = 'wp_';


    /**
     * @var string. Wordpress DB CdbConnection
     */
    private static $wordpress_db = null;


    /**
     * Return Wordpress prefix used for database tables
     */
    public function get_wordpress_prefix()
    {
        $wordpress_prefix = getenv('WP_PREFIX');
        if ( empty($wordpress_prefix) )
        {
            $wordpress_prefix = $this->_wordpress_prefix;
        }

        return $wordpress_prefix;
    }


    /**
     * Get Wordpress database connection
     * @see http://www.yiiframework.com/wiki/123/multiple-database-support-in-yii/
     */
    public function getDbConnection()
    {
        return self::getWpDbConnection();
    }


    /**
     * Return the Wordpress database connectino used by active record
     */
    protected static function getWpDbConnection()
    {
        if (self::$wordpress_db !== null)
        {
            return self::$wordpress_db;
        }

        self::$wordpress_db = Yii::app()->wordpress_db;
        if (self::$wordpress_db instanceof \CDbConnection)
        {
            Yii::app()->db->setActive(false);
            self::$wordpress_db->setActive(true);
            return self::$wordpress_db;
        }
        else
        {
            throw new \CDbException(Yii::t('yii','Wordpress Active Record requires a "db" CDbConnection application component.'));
        }
    }
}

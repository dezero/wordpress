<?php
/**
 * WordpressTrait contains custom methods to work with Wordpress data
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dzlab\wordpress\components;

use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\helpers\WordpressHelper;

use Yii;

trait WordpressTrait
{
    /**
     * @var array. Wordpress attributes. Usually from Wordpress meta models
     */
    private $_wp_attributes = [];


    /**
     * Get the value of a Wordpress field (attribute)
     */
    public function get_wp_attribute($wp_attribute_name)
    {
        // Load meta data
        $this->load_meta_data();

        return isset($this->_wp_attributes[$wp_attribute_name]) ? $this->_wp_attributes[$wp_attribute_name] : null;
    }


    /**
     * Get all the Wordpress attributes
     */
    public function get_all_wp_attributes()
    {
        // Load meta data
        $this->load_meta_data();

        return $this->_wp_attributes;
    }


    /**
     * Alias of WordpressTrait::get_wp_attribute() method
     */
    public function get_field($field_name)
    {
        return $this->get_wp_attribute($field_name);
    }


    /**
     * Alias of WordpressTrait::get_wp_attribute() method
     */
    public function get_all_fields()
    {
        return $this->get_all_wp_attributes();
    }


    /**
     * Load meta data from WpUserMeta models
     */
    public function load_meta_data($is_force_reload = false)
    {
        if ( empty($this->_wp_attributes) || $is_force_reload )
        {
            $this->_wp_attributes = [];
            if ( $this->metaData )
            {
                foreach ( $this->metaData as $wp_meta_model )
                {
                    $this->_wp_attributes[$wp_meta_model->meta_key] = $this->get_wp_value($wp_meta_model->meta_value);
                }
            }
        }

        return true;
    }


    /**
     * Get Wordpress field value checking if it's a serialized value
     */
    public function get_wp_value($wp_value)
    {
        if ( empty($wp_value) )
        {
            return '';
        }

        if ( $wp_value === "true" || $wp_value === "false" )
        {
            return (bool) $wp_value;
        }
        
        return WordpressHelper::maybe_unserialize($wp_value);
    }


    /**
     * Set a Wordpress value checking befor if it's a serialized value
     */
    public function set_wp_value($wp_value)
    {
        return WordpressHelper::maybe_serialize($wp_value);
    }


    /**
     * Alias of WordpressTrait::save_meta() method
     */
    public function save_field($field_name, $field_value)
    {
        if ( method_exists($this, 'save_meta') )
        {
            return $this->save_meta($field_name, $field_value);
        }

        return false;
    }
}

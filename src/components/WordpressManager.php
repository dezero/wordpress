<?php
/**
 * Wordpress Manager
 * 
 * Helper classes to work with Wordpress models
 */

namespace dzlab\wordpress\components;

use dz\base\ApplicationComponent;
use dz\helpers\StringHelper;
use dzlab\wordpress\helpers\WordpressHelper;
use dzlab\wordpress\models\WpOption;
use dzlab\wordpress\models\WpPost;
use dzlab\wordpress\models\WpUser;
use user\models\User;
use Yii;

class WordpressManager extends ApplicationComponent
{
    /**
     * Wordpres user prefix for user models (username attribute)
     */
    public $wordpress_prefix = 'wp_';


    /**
     * Wordpress URL
     */
    public $wordpress_url;


    /**
     * Return base URL of Wordpress site
     */
    public function site_url()
    {
        $wordpress_url = $this->wordpress_url;
        if ( isset(Yii::app()->params['wordpress_url']) )
        {
            $wordpress_url = Yii::app()->params['wordpress_url'];
        }

        return $wordpress_url;
    }


    /**
     * Return an option value
     */
    public function get_option($option_name)
    {
        $wp_option_model = WpOption::model()->findByName($option_name);
        if ( ! $wp_option_model )
        {
            return null;
        }

        return $wp_option_model->get_wp_value($wp_option_model->option_value);
    }


    /**
     * Save a new option value
     */
    public function save_option($option_name, $option_value)
    {
        $wp_option_model = WpOption::model()->findByName($option_name);
        if ( ! $wp_option_model )
        {
            return false;
        }

        return $wp_option_model->save_option($option_value);
    }


    /**
     * Get ACF fields: groups and fields
     */
    public function get_custom_fields()
    {
        $vec_output = [];

        // Get all ACF groups
        $vec_acf_group_models = WpPost::get()
            ->where(['post_type' => 'acf-field-group'])
            ->all();

        if ( !empty($vec_acf_group_models) )
        {
            foreach ( $vec_acf_group_models as $wp_post_acf_group_model )
            {
                $vec_output[$wp_post_acf_group_model->post_name] = [
                    'post_id'   => $wp_post_acf_group_model->ID,
                    'acf_group' => $wp_post_acf_group_model->post_name,
                    'status'    => $wp_post_acf_group_model->post_status,
                    'title'     => $wp_post_acf_group_model->post_title,
                    'settings'  => $wp_post_acf_group_model->get_wp_value($wp_post_acf_group_model->post_content),
                    'fields'    => []
                ];

                // Get all ACF fields
                if ( $wp_post_acf_group_model->children )
                {
                    foreach ( $wp_post_acf_group_model->children as $wp_post_acf_field_model )
                    {
                        $vec_output[$wp_post_acf_field_model->post_name] = [
                            'post_id'       => $wp_post_acf_field_model->ID,
                            'acf_id'        => $wp_post_acf_field_model->post_name,
                            'group_post_id' => $wp_post_acf_group_model->ID,
                            'group_acf_id'  => $wp_post_acf_group_model->post_name,
                            'status'        => $wp_post_acf_field_model->post_status,
                            'title'         => $wp_post_acf_field_model->post_title,
                            'settings'      => $wp_post_acf_field_model->get_wp_value($wp_post_acf_field_model->post_content),
                        ];

                        $vec_output[$wp_post_acf_group_model->post_name]['fields'][] = $wp_post_acf_field_model->post_name;
                    }
                }
            }
        }

        return $vec_output;
    }


    /**
     * Find a WpPost model
     */
    public function load_post_model($wp_post_id, $wp_post_type = null)
    {
        if ( $wp_post_type !== null )
        {
            return WpPost::get()->where([
                'ID'        => $wp_post_id,
                'post_type' => $wp_post_type
            ])->one();
        }
        
        return WpPost::findOne($wp_post_id);
    }


    /**
     * Get User model (from Dz Framework)
     * 
     * @param integer. $wp_user_id Wordpress user id
     * @param bool. $is_sync Create/update from Wordpress to Dz Framework
     * @param string. $wp_user_attribute Attribute name on "user_users" Dz Framework table
     * @return User model
     */
    public function dz_user_model($wp_user_id, $is_sync = false, $wp_user_attribute = 'wp_user_id')
    {
        $user_model = User::get()->where([$wp_user_attribute => $wp_user_id])->one();
        
        // Sync data from Wordpress (WpUser model) to Dz Framework (User model)
        if ( $is_sync )
        {
            $wp_user_model = WpUser::findOne($wp_user_id);
            if ( $wp_user_model )
            {
                $user_model = $wp_user_model->sync_to_dz($user_model, $wp_user_attribute);
            }
        }

        return $user_model;
    }


    /**
     * Sync a user from Wordpress to Dz Framework
     */
    public function sync_user_from_wordpress($wp_user_id, $wp_user_attribute = 'wp_user_id')
    {
        return $this->dz_user_model($wp_user_id, true, $wp_user_attribute);
    }


    /**
     * Sync an User model from Dz Framework to Wordpress via API
     */
    public function sync_user_to_wordpress($user_model, $vec_attributes, $wp_user_attribute = 'wp_user_id')
    {
        // Send a request to API endpoint "POST /v2/users/{id}" --> UPDATE USER
        if ( $user_model->hasAttribute($wp_user_attribute) && !empty($user_model->getAttribute($wp_user_attribute)) )
        {
            $response = Yii::app()->wordpressApi->post_user_id($user_model->getAttribute($wp_user_attribute), $vec_attributes);
        }

        // Send a request to API endpoint "POST /v2/users" --> CREATE NEW USER
        else
        {
            $response = Yii::app()->wordpressApi->post_user($vec_attributes);
        }

        // Receive and process response
        $vec_response = Yii::app()->wordpressApi->get_response_body(true);
        if ( Yii::app()->wordpressApi->is_last_action_success() && isset($vec_response['id']) && !empty($vec_response['id']) )
        {
            $wp_user_model = WpUser::findOne($vec_response['id']);

            // Sync now from Wordpress to Dz Framework (create new WordpressUser model)
            if ( $wp_user_model )
            {
                $wp_user_model->sync_wordpress_user($user_model->id);
            }
        }

        return $vec_response;
    }


    /**
     * Sync a POST model from Dz Framework to Wordpress via API
     */
    public function sync_post_to_wordpress($entity_model, $vec_attributes, $wp_attribute = 'wp_post_id')
    {
        // Send a request to API endpoint "POST /v2/posts/{id}" --> UPDATE POST
        if ( $entity_model->hasAttribute($wp_attribute) && !empty($entity_model->getAttribute($wp_attribute)) )
        {
            $response = Yii::app()->wordpressApi->post_post_id($entity_model->getAttribute($wp_attribute), $vec_attributes);
        }

        // Send a request to API endpoint "POST /v2/posts" --> CREATE NEW POST
        else
        {
            $response = Yii::app()->wordpressApi->post_post($vec_attributes);
        }

        // Receive and process response
        $vec_response = Yii::app()->wordpressApi->get_response_body(true);
        // if ( Yii::app()->wordpressApi->is_last_action_success() && isset($vec_response['id']) && !empty($vec_response['id']) )
        // {
        // }

        return $vec_response;
    }


    /**
     * Sync a ASSET FILE model from Dz Framework to Wordpress via API
     */
    public function sync_file_to_wordpress($asset_file_model)
    {
        if ( ! $asset_file_model->load_file() )
        {
            return null;
        }

        $file_name = $asset_file_model->file_name;
        $file_mime = $asset_file_model->file_mime;
        $file_content = $asset_file_model->file->getContents();
        $response = Yii::app()->wordpressApi->post_media($file_name, $file_mime, $file_content);

        return Yii::app()->wordpressApi->get_response_body(true);
    }


    /**
     * Sync an EXTERNAL file from Dz Framework to Wordpress via API
     */
    public function sync_external_image_to_wordpress($external_file_url)
    {
        $file_name = basename($external_file_url);
        $file_mime = WordpressHelper::wp_get_image_mime($external_file_url);
        $file_content = file_get_contents($external_file_url);
        if ( empty($file_name) || empty($file_mime) || empty($file_content) )
        {
            return null;
        }

        $response = Yii::app()->wordpressApi->post_media($file_name, $file_mime, $file_content);

        return Yii::app()->wordpressApi->get_response_body(true);
    }


    /**
     * Generate an access token for Wordpress users to work with DZ API
     */
    public function api_access_token($wp_user_model, $timestamp = null)
    {
        if ( empty($wp_user_model) )
        {
            return null;
        }

        $user_model = $this->dz_user_model($wp_user_model->ID);
        if ( ! $user_model )
        {
            return null;
        }

        // Timestamp
        if ( $timestamp === null )
        {
            $timestamp = time();
        }

        // Decoded token is {user_id}--{wp_user_email}--{wp_user_id}--{verification_code}##{timestamp_d/m/Y}
        $decoded_token = $user_model->id .'--'. $wp_user_model->user_email .'--'. $wp_user_model->ID .'--'. $user_model->verification_code .'##'. date('d/m/Y', $timestamp);

        return StringHelper::encrypt($decoded_token, 'sha1');
    }


    /**
     * Check if an access token for Wordpress users is valid
     *
     * The access token expires the next day at 23:59 from being generated
     */
    public function check_access_token($wp_user_model, $access_token)
    {
         if ( empty($wp_user_model) || empty($access_token) )
        {
            return false;
        }

        $user_model = $this->dz_user_model($wp_user_model->ID);
        if ( ! $user_model )
        {
            return false;
        }

        // Check access token given TODAY as timestamp
        $generated_token_today = $this->api_access_token($wp_user_model);
        if ( $generated_token_today !== null && $generated_token_today === $access_token )
        {
            return true;
        }

        // Check access token given YESTERDAY as timestamp
        $yesterday = time() - 86400;
        $generated_token_yesterday = $this->api_access_token($wp_user_model, $yesterday);
        if ( $generated_token_yesterday !== null && $generated_token_yesterday === $access_token )
        {
            return true;
        }

        return false;
    }
}

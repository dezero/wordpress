<?php
/**
 * Wordpress API client
 *
 * Helper classes to work with Wordpress REST API
 *
 * @see https://developer.wordpress.org/rest-api/reference/
 */

namespace dzlab\wordpress\components;

// use dz\base\ApplicationComponent;
use dz\rest\RestClient;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use user\models\User;
use Yii;

class WordpressApi extends RestClient
{
    /**
     * @var string. Auth URL to create API tokens
     */
    public $auth_url;


    /**
     * @var string Last used endpoint
     */
    public $last_endpoint;


    /**
     * @var array Wordpress API configuration
     */
    protected $vec_config = [];


    /**
     * Init function
     */
    public function init()
    {
        // Wordpress API configuration
        $this->vec_config = Yii::app()->config->get('components.wordpress');

        // Auth URL to create API tokens
        $this->auth_url = $this->vec_config['auth_url'];

        // Base URL
        $this->base_url = $this->vec_config['base_url'];

        // Debug mode?
        if ( isset($this->vec_config['is_debug']) )
        {
            $this->is_debug = $this->vec_config['is_debug'];
        }
    }


    /**
     * Custom event before sending HTTP request
     */
    public function beforeSend()
    {
        if ( $this->last_endpoint !== '/v2/media' )
        {
            $this->client->setHeaders([
                'Authorization'     => 'Bearer '. $this->get_access_token(),
                'accept'            => 'application/json;charset=UTF-8',
                'Content-Type'      => 'application/json;charset=UTF-8',
            ]);
        }

        return parent::beforeSend();
    }


    /**
     * Custom event after sending HTTP request
     */
    public function afterSend()
    {
        // If some error -> SAVE LOG
        if ( ! $this->is_last_action_success() )
        {
            $this->save_log('wp_api');
        }

        // Register success requests
        else if ( $this->is_debug )
        {
            $this->save_log('wp_api');
        }

        // Save all POST and PUT requests on database
        if ( $this->method !== 'GET' && $this->last_endpoint !== '/v2/media' )
        {
            $this->save_db_log('wordpress');
        }
    }


    /**
     * Return last Wordpress API error
     */
    /*
    public function get_last_error()
    {
        if ( !empty($this->last_error) && !is_array($this->last_error) && ! $this->is_last_action_success() && $this->response )
        {
            // HTTP error - For example {code: 403, message: "Forbidden"}
            $this->last_error = [
                'code'      => $this->response->getStatus(),
                'message'   => $this->response->getMessage()
            ];
        }

        return $this->last_error;
    }
    */


    /**
     * {@inheritdoc}
     */
    public function get_response_body($is_json = false)
    {
        if ( $this->response )
        {
            if ( $is_json )
            {
                return Json::decode($this->response->getBody());
            }

            return $this->response->getBody();
        }

        return null;
    }


    /**
     * Returns a list of Wordpres users
     *
     * - API REST Endpoint "GET /v2/users"
     */
    public function get_users($vec_input = [])
    {
        // Endpoint and entity information
        $this->entity_type = 'WpUser';
        $endpoint_uri = '/v2/users';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri, $vec_input);
    }


    /**
     * Creates a new Wordpress user
     *
     * - API REST Endpoint "POST /v2/user"
     */
    public function post_user($vec_input = [])
    {
        // Endpoint and entity information
        $this->entity_type = 'WpUser';
        $endpoint_uri = '/v2/users';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }


    /**
     * Updates a Wordpress user
     *
     * - API REST Endpoint "POST /v2/users/{id}"
     */
    public function post_user_id($id, $vec_input = [])
    {
        // Endpoint and entity information
        $this->entity_type = 'WpUser';
        $this->entity_id = $id;
        $endpoint_uri = '/v2/users/'. $id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }


    /**
     * Deletes a Wordpress user
     *
     * - API REST Endpoint "DELETE /v2/users/{id}"
     */
    public function delete_user($id, $vec_input = [])
    {
        // Endpoint and entity information
        $this->entity_type = 'WpUser';
        $this->entity_id = $id;
        $endpoint_uri = '/v2/users/'. $id;

        // Required input parameters: "reassign" and "force"
        if ( ! isset($vec_input['reassign']) )
        {
            $vec_input['reassign'] = 1;
            $vec_input['force'] = 1;
        }

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->delete($endpoint_uri, $vec_input);
    }


    /**
     * Returns a list of Wordpres posts
     *
     * - API REST Endpoint "GET /v2/posts"
     */
    public function get_posts($vec_input = [], $entity_type = 'WpPost')
    {
        // Endpoint and entity information
        $this->entity_type = $entity_type;
        $endpoint_uri = '/v2/posts';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->get($endpoint_uri, $vec_input);
    }


    /**
     * Creates a new Wordpress post
     *
     * - API REST Endpoint "POST /v2/posts"
     */
    public function post_post($vec_input = [], $entity_type = 'WpPost')
    {
        // Endpoint and entity information
        $this->entity_type = $entity_type;
        $endpoint_uri = '/v2/posts';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }


    /**
     * Updates a Wordpress post
     *
     * - API REST Endpoint "POST /v2/posts/{id}"
     */
    public function post_post_id($id, $vec_input = [], $entity_type = 'WpPost')
    {
        // Endpoint and entity information
        $this->entity_type = $entity_type;
        $this->entity_id = $id;
        $endpoint_uri = '/v2/posts/'. $id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $vec_input);
    }


    /**
     * Deletes a Wordpress post
     *
     * - API REST Endpoint "DELETE /v2/posts/{id}"
     */
    public function delete_post($id, $vec_input = [], $entity_type = 'WpPost')
    {
        // Endpoint and entity information
        $this->entity_type = $entity_type;
        $this->entity_id = $id;
        $endpoint_uri = '/v2/posts/'. $id;

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->delete($endpoint_uri, $vec_input);
    }


    /**
     * Upload a file/image to Wordpress
     *
     * - API REST Endpoint "POST /v2/media"
     *
     * @see https://rudrastyh.com/wordpress/upload-featured-image-rest-api.html
     */
    public function post_media($file_name, $file_mime, $file_content, $entity_type = 'WpMedia')
    {
        $vec_headers = [
            'Authorization'         => 'Bearer '. $this->get_access_token(),
            'accept'                => 'application/json;charset=UTF-8',
            'Content-Type'          => $file_mime,
            'Content-Disposition'   => 'attachment; filename="' . $file_name . '"',
        ];

        $endpoint_uri = '/v2/media';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send request
        return $this->post($endpoint_uri, $file_content, $vec_headers);
    }


    /**
     * Return languages from Polylang Pro plugin
     *
     * - API REST Endpoint "GET /pll/v1/languages"
     *
     * @see https://polylang.pro/doc/rest-api/
     */
    public function get_pll_languages()
    {
        // We need to rewrite base_url because we don't need "wp/" int URL
        $this->base_url =  getenv('WP_URL') . 'wp-json';

        $endpoint_uri = '/pll/v1/languages';

        // Save last used endpoint
        $this->last_endpoint = $endpoint_uri;

        // Send a request to API endpoint "GET /pll/v1/languages"
        return $this->get($endpoint_uri);
    }


    /**
     * Return access token (JWT)
     */
    public function get_access_token()
    {
        if ( empty($this->auth_key) )
        {
            $vec_access_token = $this->request_access_token();
            if ( !empty($vec_access_token) && isset($vec_access_token['token']) )
            {
                $this->auth_key = $vec_access_token['token'];
            }
        }

        return $this->auth_key;
    }


    /**
     * Return JWT (Json Web Token)
     */
    public function request_access_token()
    {
        // Build URL and create HTTP Client
        $client = new \EHttpClient($this->auth_url);
        $client->setMethod('POST');
        $client->setHeaders([
            'accept'        => 'application/json;charset=UTF-8',
            'Content-Type'  => 'application/json'
        ]);
        $client->setParameterPost([
            'username'  => $this->vec_config['api_username'],
            'password'  => $this->vec_config['api_password']
        ]);

        $response = $client->request();
        if ( $response->isSuccessful() )
        {
            if ( $this->is_debug )
            {
                Log::wp_api('Requesting new token: OK - '. trim($response->getBody()));
            }

            return Json::decode($response->getBody());
        }

        Log::wp_api('Requesting new token: ERROR - '. trim($response->getRawBody()));

        return null;
    }
}

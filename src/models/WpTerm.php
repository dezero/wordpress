<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpTerm as BaseWpTerm;
use dzlab\wordpress\models\WpTaxonomy;
use dzlab\wordpress\models\WpTermMeta;
use user\models\User;
use Yii;

/**
 * WpTerm model class for "wp_terms" database table
 *
 * Columns in table "wp_terms" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $term_id
 * @property string $name
 * @property string $slug
 * @property integer $term_group
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpTerm extends BaseWpTerm
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['term_group', 'numerical', 'integerOnly' => true],
            ['name, slug', 'length', 'max'=> 200],
            ['name, slug, term_group', 'default', 'setOnEmpty' => true, 'value' => null],
            ['term_id, name, slug, term_group', 'safe', 'on' => 'search'],
        ];
    }
    

    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'metaData' => [self::HAS_MANY, WpTermMeta::class, 'term_id', 'order' => 'metaData.meta_id ASC'],
            'taxonomies' => [self::HAS_MANY, WpTaxonomy::class, 'term_id', 'order' => 'taxonomies.term_taxonomy_id ASC'],

            // Custom relations
        ];
    }


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

    
    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'term_id' => Yii::t('app', 'Term'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'term_group' => Yii::t('app', 'Term Group'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.term_id', $this->term_id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.slug', $this->slug, true);
        $criteria->compare('t.term_group', $this->term_group);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['term_id' => true]]
        ]);
    }


    /**
     * WpTerm models list
     * 
     * @return array
     */
    public function wpterm_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['term_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpTerm::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('term_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Create or update a WpTermMeta model for this model
     */
    public function save_meta($meta_key, $meta_value)
    {
        // Check if "meta_key" exists
        $wp_term_meta_model = null;
        if ( $this->metaData )
        {
            foreach ( $this->metaData as $wp_meta_model )
            {
                if ( $wp_meta_model->meta_key === $meta_key )
                {
                    $wp_term_meta_model = $wp_meta_model;
                }
            }
        }

        // Create a new WpTermMeta model
        if ( $wp_term_meta_model === null )
        {
            $wp_term_meta_model = Yii::createObject(WpTermMeta::class);
        }

        // Set attributes
        $wp_term_meta_model->setAttributes([
            'term_id'       => $this->term_id,
            'meta_key'      => $meta_key,
            'meta_value'    => $this->set_wp_value($meta_value),
        ]);

        if ( ! $wp_term_meta_model->save() )
        {
            Log::save_model_error($wp_term_meta_model);

            return false;
        }

        return true;
    }


    /**
     * Get all the related posts
     */
    public function get_posts()
    {
        $vec_post_models = [];

        if ( $this->taxonomies )
        {
            foreach ( $this->taxonomies as $taxonomy_model )
            {
                if ( $taxonomy_model->relationships )
                {
                    foreach ( $taxonomy_model->relationships as $term_taxonomy_model )
                    {
                        if ( $term_taxonomy_model->post )
                        {
                            $vec_post_models[$term_taxonomy_model->object_id] = $term_taxonomy_model->post;
                        }
                    }
                }
            }
        }

        return $vec_post_models;
    }
}

<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpUser as BaseWpUser;
use dzlab\wordpress\models\WordpressUser;
use dzlab\wordpress\models\WpPost;
use dzlab\wordpress\models\WpUserMeta;
use user\models\User;
use Yii;

/**
 * WpUser model class for "wp_users" database table
 *
 * Columns in table "wp_users" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $ID
 * @property string $user_login
 * @property string $user_pass
 * @property string $user_nicename
 * @property string $user_email
 * @property string $user_url
 * @property string $user_registered
 * @property string $user_activation_key
 * @property integer $user_status
 * @property string $display_name
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpUser extends BaseWpUser
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['user_status', 'numerical', 'integerOnly' => true],
            ['user_login', 'length', 'max'=> 60],
            ['user_pass, user_activation_key', 'length', 'max'=> 255],
            ['user_nicename', 'length', 'max'=> 50],
            ['user_email, user_url', 'length', 'max'=> 100],
            ['display_name', 'length', 'max'=> 250],
            ['user_login, user_pass, user_nicename, user_email, user_url, user_registered, user_activation_key, user_status, display_name', 'default', 'setOnEmpty' => true, 'value' => null],
            ['user_registered', 'safe'],
            ['ID, user_login, user_pass, user_nicename, user_email, user_url, user_registered, user_activation_key, user_status, display_name', 'safe', 'on' => 'search'],
        ];
    }
    

    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'metaData' => [self::HAS_MANY, WpUserMeta::class, 'user_id', 'order' => 'metaData.umeta_id ASC'],
            'posts' => [self::HAS_MANY, WpPost::class, 'post_author', 'order' => 'posts.ID DESC'],
            'comments' => [self::HAS_MANY, WpComment::class, 'user_id', 'order' => 'comments.ID DESC'],

            // Custom relations
        ];
    }


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

    
    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'user_login' => Yii::t('app', 'User Login'),
            'user_pass' => Yii::t('app', 'User Pass'),
            'user_nicename' => Yii::t('app', 'User Nicename'),
            'user_email' => Yii::t('app', 'User Email'),
            'user_url' => Yii::t('app', 'User Url'),
            'user_registered' => Yii::t('app', 'User Registered'),
            'user_activation_key' => Yii::t('app', 'User Activation Key'),
            'user_status' => Yii::t('app', 'User Status'),
            'display_name' => Yii::t('app', 'Display Name'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.ID', $this->ID);
        $criteria->compare('t.user_login', $this->user_login, true);
        $criteria->compare('t.user_pass', $this->user_pass, true);
        $criteria->compare('t.user_nicename', $this->user_nicename, true);
        $criteria->compare('t.user_email', $this->user_email, true);
        $criteria->compare('t.user_url', $this->user_url, true);
        $criteria->compare('t.user_registered', $this->user_registered, true);
        $criteria->compare('t.user_activation_key', $this->user_activation_key, true);
        $criteria->compare('t.user_status', $this->user_status);
        $criteria->compare('t.display_name', $this->display_name, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['ID' => true]]
        ]);
    }


    /**
     * WpUser models list
     * 
     * @return array
     */
    public function wpuser_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['ID', 'user_login'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpUser::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('ID')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Create or update a WpUserMeta model for this model
     */
    public function save_meta($meta_key, $meta_value)
    {
        // Check if "meta_key" exists
        $wp_user_meta_model = null;
        if ( $this->metaData )
        {
            foreach ( $this->metaData as $wp_meta_model )
            {
                if ( $wp_meta_model->meta_key === $meta_key )
                {
                    $wp_user_meta_model = $wp_meta_model;
                }
            }
        }

        // Create a new WpUserMeta model
        if ( $wp_user_meta_model === null )
        {
            $wp_user_meta_model = Yii::createObject(WpUserMeta::class);
        }

        // Set attributes
        $wp_user_meta_model->setAttributes([
            'user_id'       => $this->ID,
            'meta_key'      => $meta_key,
            'meta_value'    => $this->set_wp_value($meta_value),
        ]);

        if ( ! $wp_user_meta_model->save() )
        {
            Log::save_model_error($wp_user_meta_model);

            return false;
        }

        return true;
    }


    /**
     * Check if the given $session_token exists on WP database for current user
     */
    public function check_token($session_token)
    {
        $vec_session_tokens = $this->get_session_tokens();
        if ( !empty($vec_session_tokens) )
        {
            return isset($vec_session_tokens[$session_token]);
        }

        return false;
    }


    /**
     * Return session tokens from UserMeta model
     */
    public function get_session_tokens()
    {
        return $this->get_field('session_tokens');
    }


    /**
     * Get Wordpress roles
     */
    public function get_roles()
    {
        return [];
    }


    /**
     * Check if user has assigned a given role name
     */
    public function has_role($role_name)    
    {
        return false;
    }


    /**
     * Returns the mail tokens
     */
    public function mailTokens()
    {
        return [
            '{wp_user.id}'            => $this->ID,
            '{wp_user.email}'         => $this->user_email,
            '{wp_user.name}'          => $this->display_name,
        ];
    }


    /**
     * Help text for the mail tokens
     */
    public function mailHelp()
    {
        return [
            '{wp_user.id}'             => 'User ID (internal)',
            '{wp_user.email}'          => 'Email address',
            '{wp_user.name}'           => 'Display name',
        ];
    }


    /**
     * Sync Wordpress user (WpUser model) to Dz Framework (User model)
     */
    public function sync_to_dz($user_model = null, $wp_user_attribute = 'wp_user_id', $vec_attributes = [])
    {
        // Try to get firstname and lastname
        $vec_name = $this->process_display_name();

        // Create new user
        if ( ! $user_model )
        {
            // Random password (used as "sync_code")
            $sync_code = StringHelper::random_string(24);

            // User model
            $user_model = Yii::createObject(User::class);
            $vec_attributes = \CMap::mergeArray([
                // 'username'          => Yii::app()->wordpressManager->wordpress_prefix . $sync_code,
                'username'          => $this->user_login,
                'email'             => $this->user_email,
                'firstname'         => $vec_name['firstname'],
                'lastname'          => $vec_name['lastname'],
                'password'          => $sync_code,
                'verify_password'   => $sync_code,
                'verification_code' => StringHelper::encrypt(microtime() . $sync_code),
                'status_type'       => 'active',
                $wp_user_attribute  => $this->ID,
            ], $vec_attributes);
            $user_model->setAttributes($vec_attributes);
            
            // Validate and create new User model
            if ( $user_model->validate() )
            {
                $user_model->password = StringHelper::encrypt($user_model->password);
                $user_model->save(false);

                // Custom event "afterNewSync"
                $this->afterNewSync($user_model, $sync_code);
            }
            else
            {
                Log::wp_sync('ERROR - Wordpress user #'. $this->ID .' - '. $this->user_login .' could not be synced (CREATED). '. print_r($user_model->getErrors(), true));
            }
        }

        // Update user
        else
        {
            // Update User model
            $vec_attributes = \CMap::mergeArray([
                'email'             => $this->user_email,
                'firstname'         => $vec_name['firstname'],
                'lastname'          => $vec_name['lastname'],
                $wp_user_attribute  => $this->ID,
            ], $vec_attributes);
            $user_model->setAttributes($vec_attributes);

            if ( $user_model->save() )
            {
                // Update WordpressUser model
                $wordpress_user_model = $this->sync_wordpress_user($user_model->id);
            }
            else
            {
                Log::wp_sync('ERROR - Wordpress user #'. $this->ID .' - '. $this->user_login .' could not be synced (UPDATED). '. print_r($user_model->getErrors(), true));
            }
        }

        // Reload User model
        if ( $user_model )
        {
            $user_model = User::model()->cache(0)->findByPk($user_model->id);
        }

        return $user_model;
    }


    /**
     * Called after a new User model has been created from WP sync process
     */
    public function afterNewSync($user_model, $sync_code)
    {
        // Create new WordpressUser model
        $wordpress_user_model = $this->sync_wordpress_user($user_model->id, $sync_code);

        // Log sync
        Log::wp_sync('Wordpress user #'. $this->ID .' - '. $this->user_login .' ('. $this->user_email .') has been synced.');

        return true;
    }


    /**
     * Returns firstname and lastname from "display_name" WP attribute
     */
    public function process_display_name()
    {
        $vec_output = [
            'firstname' => $this->display_name,
            'lastname'  => ''
        ];

        $vec_name = explode(' ', $this->display_name);
        if ( count($vec_name) > 1 )
        {
            $vec_output['firstname'] = $vec_name[0];
            unset($vec_name[0]);
            $vec_output['lastname'] = implode(' ', $vec_name);
        }

        return $vec_output;
    }


    /**
     * Sync from WpUser model (Wordpress) to WordpressUser model (Dz Framework)
     * 
     * ATTENTION: WordpressUser model belongs to Dz Framwework
     */
    public function sync_wordpress_user($user_id, $sync_code = '')
    {
        $wordpress_user_model = WordpressUser::get()->where(['user_id' => $user_id])->one();

        // Create new WordpressUser model
        if ( ! $wordpress_user_model )
        {
            $wordpress_user_model = new WordpressUser;
            $wordpress_user_model->setAttributes([
                'user_id'               => $user_id,
                'wp_user_id'            => $this->ID
            ]);
        }

        $wordpress_user_model->setAttributes([
            'sync_code'             => !empty($sync_code) ? $sync_code : null,
            'wp_user_login'         => $this->user_login,
            'wp_user_nicename'      => $this->user_nicename,
            'wp_user_email'         => $this->user_email,
            'wp_user_registered'    => strtotime(date($this->user_registered)),
            'wp_user_status'        => $this->user_status,
            'wp_display_name'       => $this->display_name
        ]);

        if ( ! $wordpress_user_model->save() )
        {
            Log::save_model_error($wordpress_user_model);

            return null;
        }

        return $wordpress_user_model;
    }
}

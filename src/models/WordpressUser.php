<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WordpressUser as BaseWordpressUser;
use dzlab\wordpress\models\WpUser;
use user\models\User;
use Yii;

/**
 * WordpressUser model class for "wordpress_user" database table
 *
 * Columns in table "wordpress_user" available as properties of the model,
 * followed by relations of table "wordpress_user" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $wp_user_id
 * @property integer $user_id
 * @property string $sync_code
 * @property string $wp_user_login
 * @property string $wp_user_nicename
 * @property string $wp_user_email
 * @property integer $wp_user_registered
 * @property integer $wp_user_status
 * @property string $wp_display_name
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $updatedUser
 * @property mixed $user
 */
class WordpressUser extends BaseWordpressUser
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['wp_user_id, user_id, wp_user_login, wp_user_nicename, wp_user_email, wp_user_registered, wp_display_name, created_date, created_uid, updated_date, updated_uid', 'required'],
			['wp_user_id, user_id, wp_user_registered, wp_user_status, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['sync_code', 'length', 'max'=> 255],
			['wp_user_login', 'length', 'max'=> 60],
			['wp_user_nicename', 'length', 'max'=> 50],
			['wp_user_email', 'length', 'max'=> 100],
			['wp_display_name', 'length', 'max'=> 250],
			['sync_code, wp_user_status', 'default', 'setOnEmpty' => true, 'value' => null],
			['wp_user_id, user_id, sync_code, wp_user_login, wp_user_nicename, wp_user_email, wp_user_registered, wp_user_status, wp_display_name, created_date, created_uid, updated_date, updated_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'user' => [self::BELONGS_TO, User::class, 'user_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
            'wpUser' => [self::BELONGS_TO, WpUser::class, 'wp_user_id'],
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'wp_user_registered' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'wp_user_id' => Yii::t('app', 'Wp User'),
			'user_id' => null,
			'sync_code' => Yii::t('app', 'Sync Code'),
			'wp_user_login' => Yii::t('app', 'Wp User Login'),
			'wp_user_nicename' => Yii::t('app', 'Wp User Nicename'),
			'wp_user_email' => Yii::t('app', 'Wp User Email'),
			'wp_user_registered' => Yii::t('app', 'Wp User Registered'),
			'wp_user_status' => Yii::t('app', 'Wp User Status'),
			'wp_display_name' => Yii::t('app', 'Wp Display Name'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'createdUser' => null,
			'updatedUser' => null,
			'user' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.wp_user_id', $this->wp_user_id);
        $criteria->compare('t.sync_code', $this->sync_code, true);
        $criteria->compare('t.wp_user_login', $this->wp_user_login, true);
        $criteria->compare('t.wp_user_nicename', $this->wp_user_nicename, true);
        $criteria->compare('t.wp_user_email', $this->wp_user_email, true);
        $criteria->compare('t.wp_user_registered', $this->wp_user_registered);
        $criteria->compare('t.wp_user_status', $this->wp_user_status);
        $criteria->compare('t.wp_display_name', $this->wp_display_name, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['wp_user_id' => true]]
        ]);
    }


    /**
     * WordpressUser models list
     * 
     * @return array
     */
    public function wordpressuser_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['wp_user_id', 'wp_user_login'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WordpressUser::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('wp_user_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}

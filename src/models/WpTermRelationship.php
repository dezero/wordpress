<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpTermRelationship as BaseWpTermRelationship;
use dzlab\wordpress\models\WpPost;
use dzlab\wordpress\models\WpTaxonomy;
use user\models\User;
use Yii;

/**
 * WpTermRelationship model class for "wp_term_relationships" database table
 *
 * Columns in table "wp_term_relationships" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $object_id
 * @property integer $term_taxonomy_id
 * @property integer $term_order
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpTermRelationship extends BaseWpTermRelationship
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['object_id, term_taxonomy_id, term_order', 'numerical', 'integerOnly' => true],
            ['object_id, term_taxonomy_id, term_order', 'default', 'setOnEmpty' => true, 'value' => null],
            ['object_id, term_taxonomy_id, term_order', 'safe', 'on' => 'search'],
        ];
    }
    

    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'post' => [self::BELONGS_TO, WpPost::class, 'object_id'],
            'taxonomy' => [self::BELONGS_TO, WpTaxonomy::class, 'term_taxonomy_id'],
            // 'link' => [self::BELONGS_TO, WpLink::class, 'object_id'],

            // Custom relations
        ];
    }


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

    
    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'object_id' => Yii::t('app', 'Object'),
            'term_taxonomy_id' => Yii::t('app', 'Term Taxonomy'),
            'term_order' => Yii::t('app', 'Term Order'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.object_id', $this->object_id);
        $criteria->compare('t.term_taxonomy_id', $this->term_taxonomy_id);
        $criteria->compare('t.term_order', $this->term_order);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['object_id' => true]]
        ]);
    }


    /**
     * WpTermRelationship models list
     * 
     * @return array
     */
    public function wptermrelationship_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['object_id', 'object_id', 'term_taxonomy_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpTermRelationship::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('object_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}
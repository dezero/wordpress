<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpYoastSeoLinks as BaseWpYoastSeoLinks;
use user\models\User;
use Yii;

/**
 * WpYoastSeoLinks model class for "wp_yoast_seo_links" database table
 *
 * Columns in table "wp_yoast_seo_links" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $id
 * @property string $url
 * @property integer $post_id
 * @property integer $target_post_id
 * @property string $type
 * @property integer $indexable_id
 * @property integer $target_indexable_id
 * @property integer $height
 * @property integer $width
 * @property integer $size
 * @property string $language
 * @property string $region
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpYoastSeoLinks extends BaseWpYoastSeoLinks
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['url, post_id, target_post_id, type', 'required'],
			['post_id, target_post_id, indexable_id, target_indexable_id, height, width, size', 'numerical', 'integerOnly' => true],
			['url', 'length', 'max'=> 255],
			['type', 'length', 'max'=> 8],
			['language, region', 'length', 'max'=> 32],
			['indexable_id, target_indexable_id, height, width, size, language, region', 'default', 'setOnEmpty' => true, 'value' => null],
			['id, url, post_id, target_post_id, type, indexable_id, target_indexable_id, height, width, size, language, region', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'url' => Yii::t('app', 'Url'),
			'post_id' => Yii::t('app', 'Post'),
			'target_post_id' => Yii::t('app', 'Target Post'),
			'type' => Yii::t('app', 'Type'),
			'indexable_id' => Yii::t('app', 'Indexable'),
			'target_indexable_id' => Yii::t('app', 'Target Indexable'),
			'height' => Yii::t('app', 'Height'),
			'width' => Yii::t('app', 'Width'),
			'size' => Yii::t('app', 'Size'),
			'language' => Yii::t('app', 'Language'),
			'region' => Yii::t('app', 'Region'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.url', $this->url, true);
        $criteria->compare('t.post_id', $this->post_id);
        $criteria->compare('t.target_post_id', $this->target_post_id);
        $criteria->compare('t.type', $this->type, true);
        $criteria->compare('t.indexable_id', $this->indexable_id);
        $criteria->compare('t.target_indexable_id', $this->target_indexable_id);
        $criteria->compare('t.height', $this->height);
        $criteria->compare('t.width', $this->width);
        $criteria->compare('t.size', $this->size);
        $criteria->compare('t.language', $this->language, true);
        $criteria->compare('t.region', $this->region, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['id' => true]]
        ]);
    }


    /**
     * WpYoastSeoLinks models list
     * 
     * @return array
     */
    public function wpyoastseolinks_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['id', 'url'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpYoastSeoLinks::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}
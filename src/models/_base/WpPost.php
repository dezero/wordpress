<?php
/**
 * @package dzlab\wordpress\models\_base
 */

namespace dzlab\wordpress\models\_base;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\wordpress\components\ActiveRecord;
use user\models\User;
use Yii;

/**
 * DO NOT MODIFY THIS FILE! It is automatically generated by Gii.
 * If any changes are necessary, you must set or override the required
 * property or method in class "dzlab\wordpress\models\WpPost".
 *
 * This is the model base class for the table "wp_posts".
 * Columns in table "wp_posts" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $ID
 * @property integer $post_author
 * @property string $post_date
 * @property string $post_date_gmt
 * @property string $post_content
 * @property string $post_title
 * @property string $post_excerpt
 * @property string $post_status
 * @property string $comment_status
 * @property string $ping_status
 * @property string $post_password
 * @property string $post_name
 * @property string $to_ping
 * @property string $pinged
 * @property string $post_modified
 * @property string $post_modified_gmt
 * @property string $post_content_filtered
 * @property integer $post_parent
 * @property string $guid
 * @property integer $menu_order
 * @property string $post_type
 * @property string $post_mime_type
 * @property integer $comment_count
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
abstract class WpPost extends ActiveRecord
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Returns the name of the associated database table
     */
    public function tableName()
    {
        return $this->get_wordpress_prefix() .'posts';
    }


    /**
     * Label with translation support (from GIIX)
     */
    public static function label($n = 1)
    {
        return Yii::t('app', 'WpPost|WpPosts', $n);
    }


    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['post_content, post_title, post_excerpt, to_ping, pinged, post_content_filtered', 'required'],
            ['post_author, post_parent, menu_order, comment_count', 'numerical', 'integerOnly' => true],
            ['post_status, comment_status, ping_status, post_type', 'length', 'max'=> 20],
            ['post_password, guid', 'length', 'max'=> 255],
            ['post_name', 'length', 'max'=> 200],
            ['post_mime_type', 'length', 'max'=> 100],
            ['post_author, post_date, post_date_gmt, post_status, comment_status, ping_status, post_password, post_name, post_modified, post_modified_gmt, post_parent, guid, menu_order, post_type, post_mime_type, comment_count', 'default', 'setOnEmpty' => true, 'value' => null],
            ['post_date, post_date_gmt, post_modified, post_modified_gmt', 'safe'],
            ['ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count', 'safe', 'on' => 'search'],
        ];
    }


    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
        ];
    }


    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'post_author' => Yii::t('app', 'Post Author'),
            'post_date' => Yii::t('app', 'Post Date'),
            'post_date_gmt' => Yii::t('app', 'Post Date Gmt'),
            'post_content' => Yii::t('app', 'Post Content'),
            'post_title' => Yii::t('app', 'Post Title'),
            'post_excerpt' => Yii::t('app', 'Post Excerpt'),
            'post_status' => Yii::t('app', 'Post Status'),
            'comment_status' => Yii::t('app', 'Comment Status'),
            'ping_status' => Yii::t('app', 'Ping Status'),
            'post_password' => Yii::t('app', 'Post Password'),
            'post_name' => Yii::t('app', 'Post Name'),
            'to_ping' => Yii::t('app', 'To Ping'),
            'pinged' => Yii::t('app', 'Pinged'),
            'post_modified' => Yii::t('app', 'Post Modified'),
            'post_modified_gmt' => Yii::t('app', 'Post Modified Gmt'),
            'post_content_filtered' => Yii::t('app', 'Post Content Filtered'),
            'post_parent' => Yii::t('app', 'Post Parent'),
            'guid' => Yii::t('app', 'Guid'),
            'menu_order' => Yii::t('app', 'Menu Order'),
            'post_type' => Yii::t('app', 'Post Type'),
            'post_mime_type' => Yii::t('app', 'Post Mime Type'),
            'comment_count' => Yii::t('app', 'Comment Count'),
        ];
    }


    /**
     * WpPost models list
     * 
     * @return array
     */
    public function wppost_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['ID', 'post_date'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpPost::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('ID')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.ID', $this->ID);
        $criteria->compare('t.post_author', $this->post_author);
        $criteria->compare('t.post_date', $this->post_date, true);
        $criteria->compare('t.post_date_gmt', $this->post_date_gmt, true);
        $criteria->compare('t.post_content', $this->post_content, true);
        $criteria->compare('t.post_title', $this->post_title, true);
        $criteria->compare('t.post_excerpt', $this->post_excerpt, true);
        $criteria->compare('t.post_status', $this->post_status, true);
        $criteria->compare('t.comment_status', $this->comment_status, true);
        $criteria->compare('t.ping_status', $this->ping_status, true);
        $criteria->compare('t.post_password', $this->post_password, true);
        $criteria->compare('t.post_name', $this->post_name, true);
        $criteria->compare('t.to_ping', $this->to_ping, true);
        $criteria->compare('t.pinged', $this->pinged, true);
        $criteria->compare('t.post_modified', $this->post_modified, true);
        $criteria->compare('t.post_modified_gmt', $this->post_modified_gmt, true);
        $criteria->compare('t.post_content_filtered', $this->post_content_filtered, true);
        $criteria->compare('t.post_parent', $this->post_parent);
        $criteria->compare('t.guid', $this->guid, true);
        $criteria->compare('t.menu_order', $this->menu_order);
        $criteria->compare('t.post_type', $this->post_type, true);
        $criteria->compare('t.post_mime_type', $this->post_mime_type, true);
        $criteria->compare('t.comment_count', $this->comment_count);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['ID' => true]]
        ]);
    }
    
    
    /**
     * Returns fields not showed in create/update forms
     */
    public function excludedFormFields()
    {
        return [
        ];
    }


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'post_date';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->post_date;
    }
}
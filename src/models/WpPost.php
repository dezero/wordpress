<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\helpers\WordpressHelper;
use dzlab\wordpress\models\_base\WpPost as BaseWpPost;
use dzlab\wordpress\models\WpPostAttachment;
use dzlab\wordpress\models\WpPostMeta;
use dzlab\wordpress\models\WpTermRelationship;
use dzlab\wordpress\models\WpUser;
use dzlab\wordpress\models\WpYoastIndexable;
use user\models\User;
use Yii;

/**
 * WpPost model class for "wp_posts" database table
 *
 * Columns in table "wp_posts" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $ID
 * @property integer $post_author
 * @property string $post_date
 * @property string $post_date_gmt
 * @property string $post_content
 * @property string $post_title
 * @property string $post_excerpt
 * @property string $post_status
 * @property string $comment_status
 * @property string $ping_status
 * @property string $post_password
 * @property string $post_name
 * @property string $to_ping
 * @property string $pinged
 * @property string $post_modified
 * @property string $post_modified_gmt
 * @property string $post_content_filtered
 * @property integer $post_parent
 * @property string $guid
 * @property integer $menu_order
 * @property string $post_type
 * @property string $post_mime_type
 * @property integer $comment_count
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpPost extends BaseWpPost
{
    /**
     * @var array. Cached data with Wordpress taxonomies information
     */
    private $_vec_taxonomies = [];


    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['post_content, post_title, post_excerpt, to_ping, pinged, post_content_filtered', 'required'],
            ['post_author, post_parent, menu_order, comment_count', 'numerical', 'integerOnly' => true],
            ['post_status, comment_status, ping_status, post_type', 'length', 'max'=> 20],
            ['post_password, guid', 'length', 'max'=> 255],
            ['post_name', 'length', 'max'=> 200],
            ['post_mime_type', 'length', 'max'=> 100],
            ['post_author, post_date, post_date_gmt, post_status, comment_status, ping_status, post_password, post_name, post_modified, post_modified_gmt, post_parent, guid, menu_order, post_type, post_mime_type, comment_count', 'default', 'setOnEmpty' => true, 'value' => null],
            ['post_date, post_date_gmt, post_modified, post_modified_gmt', 'safe'],
            ['ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count', 'safe', 'on' => 'search'],
        ];
    }
    

    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'parent' => [self::BELONGS_TO, WpPost::class, 'post_parent'],
            'user' => [self::BELONGS_TO, WpUser::class, 'post_author'],
            'author' => [self::BELONGS_TO, WpUser::class, 'post_author'],

            'children' => [self::HAS_MANY, WpPost::class, 'post_parent', 'order' => 'children.ID ASC'],
            'comments' => [self::HAS_MANY, WpComment::class, 'comment_post_ID', 'order' => 'comments.comment_post_ID DESC'],
            'metaData' => [self::HAS_MANY, WpPostMeta::class, 'post_id', 'order' => 'metaData.meta_id ASC'],
            'taxonomies' => [self::HAS_MANY, WpTermRelationship::class, 'object_id', 'order' => 'taxonomies.term_order ASC'],
            'terms' => [self::HAS_MANY, WpTermRelationship::class, 'object_id', 'order' => 'terms.term_order ASC'],

            // Custom relations
            'attachments' => [self::HAS_MANY, WpPostAttachment::class, 'post_parent', 'condition' => 'attachments.post_type = "attachment"', 'order' => 'attachments.ID ASC'],
            'revisions' => [self::HAS_MANY, WpPost::class, 'post_parent', 'condition' => 'attachments.post_type = "revision"', 'order' => 'revisions.ID ASC'],
        ];
    }


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

    
    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'post_author' => Yii::t('app', 'Author'),
            'post_date' => Yii::t('app', 'Date'),
            'post_date_gmt' => Yii::t('app', 'Date Gmt'),
            'post_content' => Yii::t('app', 'Content'),
            'post_title' => Yii::t('app', 'Title'),
            'post_excerpt' => Yii::t('app', 'Excerpt'),
            'post_status' => Yii::t('app', 'Status'),
            'comment_status' => Yii::t('app', 'Comment Status'),
            'ping_status' => Yii::t('app', 'Ping Status'),
            'post_password' => Yii::t('app', 'Password'),
            'post_name' => Yii::t('app', 'Name'),
            'to_ping' => Yii::t('app', 'To Ping'),
            'pinged' => Yii::t('app', 'Pinged'),
            'post_modified' => Yii::t('app', 'Modified'),
            'post_modified_gmt' => Yii::t('app', 'Modified Gmt'),
            'post_content_filtered' => Yii::t('app', 'Content Filtered'),
            'post_parent' => Yii::t('app', 'Parent'),
            'guid' => Yii::t('app', 'Guid'),
            'menu_order' => Yii::t('app', 'Menu Order'),
            'post_type' => Yii::t('app', 'Type'),
            'post_mime_type' => Yii::t('app', 'Mime Type'),
            'comment_count' => Yii::t('app', 'Comment Count'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.post_type', $this->post_type);
        $criteria->compare('t.post_title', $this->post_title, true);

        // Filter by POST status: publish, private or draft
        if ( $this->post_status )
        {
            if ( is_array($this->post_status) )
            {
                $criteria->addInCondition('t.post_status', $this->post_status);
            }
            else
            {
                $criteria->compare('t.post_status', $this->post_status);
            }
        }
        else
        {
            $criteria->addInCondition('t.post_status', ['publish', 'private', 'draft']);
        }

        /*
        $criteria->compare('t.ID', $this->ID);
        $criteria->compare('t.post_author', $this->post_author);
        $criteria->compare('t.post_date', $this->post_date, true);
        $criteria->compare('t.post_date_gmt', $this->post_date_gmt, true);
        $criteria->compare('t.post_content', $this->post_content, true);
        $criteria->compare('t.post_excerpt', $this->post_excerpt, true);
        $criteria->compare('t.comment_status', $this->comment_status, true);
        $criteria->compare('t.ping_status', $this->ping_status, true);
        $criteria->compare('t.post_password', $this->post_password, true);
        $criteria->compare('t.post_name', $this->post_name, true);
        $criteria->compare('t.to_ping', $this->to_ping, true);
        $criteria->compare('t.pinged', $this->pinged, true);
        $criteria->compare('t.post_modified', $this->post_modified, true);
        $criteria->compare('t.post_modified_gmt', $this->post_modified_gmt, true);
        $criteria->compare('t.post_content_filtered', $this->post_content_filtered, true);
        $criteria->compare('t.post_parent', $this->post_parent);
        $criteria->compare('t.guid', $this->guid, true);
        $criteria->compare('t.menu_order', $this->menu_order);
        $criteria->compare('t.post_mime_type', $this->post_mime_type, true);
        $criteria->compare('t.comment_count', $this->comment_count);
        */

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['ID' => true]]
        ]);
    }


    /**
     * WpPost models list
     * 
     * @return array
     */
    public function wppost_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['ID', 'post_date'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpPost::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('ID')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Create or update a WpPostMeta model for this model
     */
    public function save_meta($meta_key, $meta_value)
    {
        // Check if "meta_key" exists
        $wp_post_meta_model = null;
        if ( $this->metaData )
        {
            foreach ( $this->metaData as $wp_meta_model )
            {
                if ( $wp_meta_model->meta_key === $meta_key )
                {
                    $wp_post_meta_model = $wp_meta_model;
                }
            }
        }

        // Create a new WpPostMeta model
        if ( $wp_post_meta_model === null )
        {
            $wp_post_meta_model = Yii::createObject(WpPostMeta::class);
        }

        // Set attributes
        $wp_post_meta_model->setAttributes([
            'post_id'       => $this->ID,
            'meta_key'      => $meta_key,
            'meta_value'    => $this->set_wp_value($meta_value),
        ]);

        if ( ! $wp_post_meta_model->save() )
        {
            Log::save_model_error($wp_post_meta_model);

            return false;
        }

        return true;
    }


    /**
     * Load all the taxonomies information related with this post
     */
    public function load_all_taxonomies($is_force_reload = false)
    {
        if ( empty($this->_vec_taxonomies) || $is_force_reload  )
        {
            $this->_vec_taxonomies = [];
            if ( $this->terms )
            {
                foreach ( $this->terms as $wp_term_relationship_model )
                {
                    if ( $wp_term_relationship_model->taxonomy && $wp_term_relationship_model->taxonomy->term )
                    {
                        $this->_vec_taxonomies[$wp_term_relationship_model->taxonomy->term_id] = [
                            'term_id'               => $wp_term_relationship_model->taxonomy->term_id,
                            'taxonomy_id'           => $wp_term_relationship_model->term_taxonomy_id,
                            'taxonomy_type'         => $wp_term_relationship_model->taxonomy->taxonomy,
                            'name'                  => $wp_term_relationship_model->taxonomy->term->name,
                            'slug'                  => $wp_term_relationship_model->taxonomy->term->slug,
                            'term_order'            => $wp_term_relationship_model->term_order,
                            'term_group'            => $wp_term_relationship_model->taxonomy->term->term_group,
                            'taxonomy_description'  => WordpressHelper::maybe_unserialize($wp_term_relationship_model->taxonomy->description),
                            'taxonomy_parent_id'    => $wp_term_relationship_model->taxonomy->parent,
                            'taxonomy_count'        => $wp_term_relationship_model->taxonomy->count,
                        ];
                    }
                }
            }
        }

        return true;
    }


    /**
     * Return all the taxonomies information related with this post
     */
    public function get_all_taxonomies($is_force_reload = false)
    {
        // Load & return the taxonomies
        $this->load_all_taxonomies();

        return $this->_vec_taxonomies;
    }


    /**
     * Alias of WpPost::get_all_taxonomies method
     */
    public function get_all_terms($is_force_reload = false)
    {
        return $this->get_all_taxonomies($is_force_reload);
    }


    /**
     * Get the terms related with this post and filter them by type
     */
    public function get_terms_by_type($taxonomy_type)
    {
        // Load the taxonomies information
        $this->load_all_taxonomies();

        $vec_terms = [];

        if ( !empty($this->_vec_taxonomies) )
        {
            foreach ( $this->_vec_taxonomies as $term_id => $que_taxonomy )
            {
                if ( $que_taxonomy['taxonomy_type'] === $taxonomy_type )
                {
                    $vec_terms[$term_id] = $que_taxonomy;
                }
            }
        }

        return $vec_terms;
    }



    /**
     * Check if current post has a related term with id = $term_id
     */
    public function is_term($term_id)
    {
        // Load the taxonomies information
        $this->load_all_taxonomies();

        return isset($this->_vec_taxonomies[$term_id]);
    }


    /**
     * Check if current post has a related term with name = $term_name
     */
    public function is_term_by_name($term_name, $taxonomy_type = null)
    {
        // Load the taxonomies information
        $this->load_all_taxonomies();

        if ( !empty($this->_vec_taxonomies) )
        {
            foreach ( $this->_vec_taxonomies as $term_id => $que_taxonomy )
            {
                if ( $que_taxonomy['name'] === $term_name && ( $taxonomy_type === null || $que_taxonomy['taxonomy_type'] === $taxonomy_type) )
                {
                    return true;
                }
            }
        }
        return false;
    }



     /**
     * Check if current post has a related term with slug = $term_slug
     */
    public function is_term_by_slug($term_slug, $taxonomy_type = null)
    {
        // Load the taxonomies information
        $this->load_all_taxonomies();

        if ( !empty($this->_vec_taxonomies) )
        {
            foreach ( $this->_vec_taxonomies as $term_id => $que_taxonomy )
            {
                if ( $que_taxonomy['slug'] === $term_slug && ( $taxonomy_type === null || $que_taxonomy['taxonomy_type'] === $taxonomy_type) )
                {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Get file attachments
     */
    public function get_attachments()
    {
        $vec_attachments = [];

        if ( $this->attachments )
        {
            foreach ( $this->attachments as $post_model )
            {
                $vec_attachments[$post_model->ID] = [
                    'post_id'       => $post_model->ID,
                    'url'           => $post_model->guid,
                    'mime_type'     => $post_model->post_mime_type,
                    'field_name'    => '',
                    '_model'        => $post_model
                ];

                // Get information from META
                $vec_post_meta_models = WpPostMeta::get()->where([
                    'post_id'       => $this->ID,
                    'meta_value'    => $post_model->ID
                ])->all();

                if ( !empty($vec_post_meta_models) )
                {
                    foreach ( $vec_post_meta_models as $post_meta_model )
                    {
                        $vec_attachments[$post_model->ID]['field_name'] = $post_meta_model->meta_key;
                    }
                }
            }
        }

        return $vec_attachments;
    }


    /**
     * Get permalink
     */
    public function get_permalink($is_check_yoast = true)
    {
        // Try to find permalink on Yoast table
        if ( $is_check_yoast )
        {
            $yoast_model = WpYoastIndexable::get()->where([
                'object_id'         => $this->ID,
                'object_type'       => 'post',
                'object_sub_type'   => $this->post_type,
            ]);

            if ( $yoast_model && !empty($yoast_model->permalink) )
            {
                return $yoast_model->permalink;
            }
        }

        // By default, we'll use "/<post_type>/<post_name>"
        return Yii::app()->wordpressManager->site_url() . $this->post_type .'/'. $this->post_name;
    }


    /**
     * Get featured image
     */
    public function get_featured_image()
    {
        return $this->get_field('_thumbnail_id');
    }


    /**
     * Get image URL
     */
    public function thumbnail_url($size_name = 'thumbnail')
    {
        $wp_thumbnail_url = '';

        // Try with "_thumbnail_id" postmeta
        $thumbnail_post_id = $this->get_field('_thumbnail_id');
        if ( !empty($thumbnail_post_id) )
        {
            $wp_post_attachment_model = WpPostAttachment::findOne($thumbnail_post_id);
            if ( $wp_post_attachment_model && $wp_post_attachment_model->is_image() )
            {
                return $wp_post_attachment_model->image_url($size_name);
            }
        }

        // Search on "attachments"
        if ( $this->attachments )
        {
            foreach ( $this->attachments as $wp_post_attachment_model )
            {
                if ( $wp_post_attachment_model->is_image() && $wp_post_attachment_model->has_size($size_name) )
                {
                    return $wp_post_attachment_model->image_url($size_name);
                }
            }
        }

        return '';
    }



    /**
     * Return first paragraph from content
     */
    public function get_first_paragraph()
    {
        if ( !empty($this->post_content) )
        {
            $vec_paragraphs = explode("<!-- wp:paragraph -->", $this->post_content);
            if ( !empty($vec_paragraphs) && isset($vec_paragraphs[1]) )
            {
                $first_paragraph = str_replace('<!-- /wp:paragraph -->', '', $vec_paragraphs[1]);
                return StringHelper::trim($first_paragraph);
            }
        }

        return $this->post_content;
    }


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'post_title';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->post_title;
    }


    /**
     * Returns the view URL for this post
     * 
     * @see \dz\helper\Url::to() method
     */
    public function url($is_permalink = true, $is_check_yoast = true)
    {
        // Permalink
        if ( $is_permalink )
        {
            return $this->get_permalink($is_check_yoast);
        }
        
        // Default link
        return Yii::app()->wordpressManager->site_url() .'?page_id='. $this->ID;
    }


    /**
     * Return the update URL on Wordpress backend
     */
    public function update_url()
    {
        return Yii::app()->wordpressManager->site_url() .'cms/wp-admin/post.php?post='. $this->ID .'&action=edit';
    }
}

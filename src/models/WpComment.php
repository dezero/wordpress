<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpComment as BaseWpComment;
use dzlab\wordpress\models\WpCommentMeta;
use dzlab\wordpress\models\WpPost;
use dzlab\wordpress\models\WpUser;
use user\models\User;
use Yii;

/**
 * WpComment model class for "wp_comments" database table
 *
 * Columns in table "wp_comments" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $comment_ID
 * @property integer $comment_post_ID
 * @property string $comment_author
 * @property string $comment_author_email
 * @property string $comment_author_url
 * @property string $comment_author_IP
 * @property string $comment_date
 * @property string $comment_date_gmt
 * @property string $comment_content
 * @property integer $comment_karma
 * @property string $comment_approved
 * @property string $comment_agent
 * @property string $comment_type
 * @property integer $comment_parent
 * @property integer $user_id
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpComment extends BaseWpComment
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['comment_author, comment_content', 'required'],
            ['comment_post_ID, comment_karma, comment_parent, user_id', 'numerical', 'integerOnly' => true],
            ['comment_author_email, comment_author_IP', 'length', 'max'=> 100],
            ['comment_author_url', 'length', 'max'=> 200],
            ['comment_approved, comment_type', 'length', 'max'=> 20],
            ['comment_agent', 'length', 'max'=> 255],
            ['comment_post_ID, comment_author_email, comment_author_url, comment_author_IP, comment_date, comment_date_gmt, comment_karma, comment_approved, comment_agent, comment_type, comment_parent, user_id', 'default', 'setOnEmpty' => true, 'value' => null],
            ['comment_date, comment_date_gmt', 'safe'],
            ['comment_ID, comment_post_ID, comment_author, comment_author_email, comment_author_url, comment_author_IP, comment_date, comment_date_gmt, comment_content, comment_karma, comment_approved, comment_agent, comment_type, comment_parent, user_id', 'safe', 'on' => 'search'],
        ];
    }
    

    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'post' => [self::BELONGS_TO, WpPost::class, 'comment_post_ID'],
            'user' => [self::BELONGS_TO, WpUser::class, 'user_id'],
            'metaData' => [self::HAS_MANY, WpCommentMeta::class, 'comment_id', 'order' => 'metaData.meta_id ASC'],

            // Custom relations
        ];
    }


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

    
    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'comment_ID' => Yii::t('app', 'Comment'),
            'comment_post_ID' => Yii::t('app', 'Comment Post'),
            'comment_author' => Yii::t('app', 'Comment Author'),
            'comment_author_email' => Yii::t('app', 'Comment Author Email'),
            'comment_author_url' => Yii::t('app', 'Comment Author Url'),
            'comment_author_IP' => Yii::t('app', 'Comment Author Ip'),
            'comment_date' => Yii::t('app', 'Comment Date'),
            'comment_date_gmt' => Yii::t('app', 'Comment Date Gmt'),
            'comment_content' => Yii::t('app', 'Comment Content'),
            'comment_karma' => Yii::t('app', 'Comment Karma'),
            'comment_approved' => Yii::t('app', 'Comment Approved'),
            'comment_agent' => Yii::t('app', 'Comment Agent'),
            'comment_type' => Yii::t('app', 'Comment Type'),
            'comment_parent' => Yii::t('app', 'Comment Parent'),
            'user_id' => Yii::t('app', 'User'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.comment_ID', $this->comment_ID);
        $criteria->compare('t.comment_post_ID', $this->comment_post_ID);
        $criteria->compare('t.comment_author', $this->comment_author, true);
        $criteria->compare('t.comment_author_email', $this->comment_author_email, true);
        $criteria->compare('t.comment_author_url', $this->comment_author_url, true);
        $criteria->compare('t.comment_author_IP', $this->comment_author_IP, true);
        $criteria->compare('t.comment_date', $this->comment_date, true);
        $criteria->compare('t.comment_date_gmt', $this->comment_date_gmt, true);
        $criteria->compare('t.comment_content', $this->comment_content, true);
        $criteria->compare('t.comment_karma', $this->comment_karma);
        $criteria->compare('t.comment_approved', $this->comment_approved, true);
        $criteria->compare('t.comment_agent', $this->comment_agent, true);
        $criteria->compare('t.comment_type', $this->comment_type, true);
        $criteria->compare('t.comment_parent', $this->comment_parent);
        $criteria->compare('t.user_id', $this->user_id);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['comment_ID' => true]]
        ]);
    }


    /**
     * WpComment models list
     * 
     * @return array
     */
    public function wpcomment_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['comment_ID', 'comment_author'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpComment::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('comment_ID')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Create or update a WpCommentMeta model for this model
     */
    public function save_meta($meta_key, $meta_value)
    {
        // Check if "meta_key" exists
        $wp_comment_meta_model = null;
        if ( $this->metaData )
        {
            foreach ( $this->metaData as $wp_meta_model )
            {
                if ( $wp_meta_model->meta_key === $meta_key )
                {
                    $wp_comment_meta_model = $wp_meta_model;
                }
            }
        }

        // Create a new WpCommentMeta model
        if ( $wp_comment_meta_model === null )
        {
            $wp_comment_meta_model = Yii::createObject(WpCommentMeta::class);
        }

        // Set attributes
        $wp_comment_meta_model->setAttributes([
            'post_id'       => $this->ID,
            'meta_key'      => $meta_key,
            'meta_value'    => $this->set_wp_value($meta_value),
        ]);

        if ( ! $wp_comment_meta_model->save() )
        {
            Log::save_model_error($wp_comment_meta_model);

            return false;
        }

        return true;
    }
}

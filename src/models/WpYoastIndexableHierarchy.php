<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpYoastIndexableHierarchy as BaseWpYoastIndexableHierarchy;
use user\models\User;
use Yii;

/**
 * WpYoastIndexableHierarchy model class for "wp_yoast_indexable_hierarchy" database table
 *
 * Columns in table "wp_yoast_indexable_hierarchy" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $indexable_id
 * @property integer $ancestor_id
 * @property integer $depth
 * @property integer $blog_id
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpYoastIndexableHierarchy extends BaseWpYoastIndexableHierarchy
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['indexable_id, ancestor_id, depth, blog_id', 'numerical', 'integerOnly' => true],
			['indexable_id, ancestor_id, depth, blog_id', 'default', 'setOnEmpty' => true, 'value' => null],
			['indexable_id, ancestor_id, depth, blog_id', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'indexable_id' => Yii::t('app', 'Indexable'),
			'ancestor_id' => Yii::t('app', 'Ancestor'),
			'depth' => Yii::t('app', 'Depth'),
			'blog_id' => Yii::t('app', 'Blog'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.indexable_id', $this->indexable_id);
        $criteria->compare('t.ancestor_id', $this->ancestor_id);
        $criteria->compare('t.depth', $this->depth);
        $criteria->compare('t.blog_id', $this->blog_id);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['indexable_id' => true]]
        ]);
    }


    /**
     * WpYoastIndexableHierarchy models list
     * 
     * @return array
     */
    public function wpyoastindexablehierarchy_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['indexable_id', 'indexable_id', 'ancestor_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpYoastIndexableHierarchy::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('indexable_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}
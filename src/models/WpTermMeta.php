<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpTermMeta as BaseWpTermMeta;
use dzlab\wordpress\models\WpTerm;
use user\models\User;
use Yii;

/**
 * WpTermMeta model class for "wp_termmeta" database table
 *
 * Columns in table "wp_termmeta" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $meta_id
 * @property integer $term_id
 * @property string $meta_key
 * @property string $meta_value
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpTermMeta extends BaseWpTermMeta
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['term_id', 'numerical', 'integerOnly' => true],
            ['meta_key', 'length', 'max'=> 255],
            ['term_id, meta_key, meta_value', 'default', 'setOnEmpty' => true, 'value' => null],
            ['meta_value', 'safe'],
            ['meta_id, term_id, meta_key, meta_value', 'safe', 'on' => 'search'],
        ];
    }
    

    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'term' => [self::BELONGS_TO, WpTerm::class, 'term_id'],

            // Custom relations
        ];
    }


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

    
    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'meta_id' => Yii::t('app', 'Meta'),
            'term_id' => Yii::t('app', 'Term'),
            'meta_key' => Yii::t('app', 'Meta Key'),
            'meta_value' => Yii::t('app', 'Meta Value'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.meta_id', $this->meta_id);
        $criteria->compare('t.term_id', $this->term_id);
        $criteria->compare('t.meta_key', $this->meta_key, true);
        $criteria->compare('t.meta_value', $this->meta_value, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['meta_id' => true]]
        ]);
    }


    /**
     * WpTermMeta models list
     * 
     * @return array
     */
    public function termmeta_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['meta_id', 'meta_key'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpTermMeta::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('meta_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}
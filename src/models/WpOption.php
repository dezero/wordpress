<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpOption as BaseWpOption;
use user\models\User;
use Yii;

/**
 * WpOption model class for "wp_options" database table
 *
 * Columns in table "wp_options" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $option_id
 * @property string $option_name
 * @property string $option_value
 * @property string $autoload
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpOption extends BaseWpOption
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['option_value', 'required'],
            ['option_name', 'length', 'max'=> 191],
            ['autoload', 'length', 'max'=> 20],
            ['option_name, autoload', 'default', 'setOnEmpty' => true, 'value' => null],
            ['option_id, option_name, option_value, autoload', 'safe', 'on' => 'search'],
        ];
    }
    

    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [

            // Custom relations
        ];
    }


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

    
    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'option_id' => Yii::t('app', 'Option'),
            'option_name' => Yii::t('app', 'Option Name'),
            'option_value' => Yii::t('app', 'Option Value'),
            'autoload' => Yii::t('app', 'Autoload'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.option_id', $this->option_id);
        $criteria->compare('t.option_name', $this->option_name, true);
        $criteria->compare('t.option_value', $this->option_value, true);
        $criteria->compare('t.autoload', $this->autoload, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['option_id' => true]]
        ]);
    }


    /**
     * WpOption models list
     * 
     * @return array
     */
    public function wpoption_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['option_id', 'option_name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpOption::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('option_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }



    /**
     * Save new value
     */
    public function save_option($new_value)
    {
        $this->option_value = $this->set_wp_value($new_value);
        if ( ! $this->save() )
        {
            Log::save_model_error($this);

            return false;
        }

        return true;
    }


    /**
     * Find a WpOption model by name
     */
    public function findByName($option_name)
    {
        return self::get()
            ->where([
                'option_name' => $option_name
            ])
            ->one();
    }
}

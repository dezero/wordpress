<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpTaxonomy as BaseWpTaxonomy;
use dzlab\wordpress\models\WpTerm;
use dzlab\wordpress\models\WpTermRelationship;
use user\models\User;
use Yii;

/**
 * WpTaxonomy model class for "wp_term_taxonomy" database table
 *
 * Columns in table "wp_term_taxonomy" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $term_taxonomy_id
 * @property integer $term_id
 * @property string $taxonomy
 * @property string $description
 * @property integer $parent
 * @property integer $count
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpTaxonomy extends BaseWpTaxonomy
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['description', 'required'],
            ['term_id, parent, count', 'numerical', 'integerOnly' => true],
            ['taxonomy', 'length', 'max'=> 32],
            ['term_id, taxonomy, parent, count', 'default', 'setOnEmpty' => true, 'value' => null],
            ['term_taxonomy_id, term_id, taxonomy, description, parent, count', 'safe', 'on' => 'search'],
        ];
    }
    

    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'parent' => [self::BELONGS_TO, WpTaxonomy::class, 'parent'],
            'term' => [self::BELONGS_TO, WpTerm::class, 'term_id'],

            // Custom relations
            'children' => [self::HAS_MANY, WpTaxonomy::class, 'parent', 'order' => 'children.ID ASC'],
            'objects' => [self::HAS_MANY, WpTermRelationship::class, 'term_taxonomy_id', 'order' => 'objects.object_id ASC'],
            'relationships' => [self::HAS_MANY, WpTermRelationship::class, 'term_taxonomy_id', 'order' => 'relationships.object_id ASC'],
        ];
    }


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

    
    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'term_taxonomy_id' => Yii::t('app', 'Term Taxonomy'),
            'term_id' => Yii::t('app', 'Term'),
            'taxonomy' => Yii::t('app', 'Taxonomy'),
            'description' => Yii::t('app', 'Description'),
            'parent' => Yii::t('app', 'Parent'),
            'count' => Yii::t('app', 'Count'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.term_taxonomy_id', $this->term_taxonomy_id);
        $criteria->compare('t.term_id', $this->term_id);
        $criteria->compare('t.taxonomy', $this->taxonomy, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.parent', $this->parent);
        $criteria->compare('t.count', $this->count);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['term_taxonomy_id' => true]]
        ]);
    }


    /**
     * WpTaxonomy models list
     * 
     * @return array
     */
    public function wptaxonomy_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['term_taxonomy_id', 'taxonomy'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpTaxonomy::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('term_taxonomy_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Get all the related posts
     */
    public function get_posts()
    {
        $vec_post_models = [];

        if ( $this->relationships )
        {
            foreach ( $this->relationships as $term_taxonomy_model )
            {
                if ( $term_taxonomy_model->post )
                {
                    $vec_post_models[$term_taxonomy_model->object_id] = $term_taxonomy_model->post;
                }
            }
        }

        return $vec_post_models;
    }
}
<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\models\_base\WpYoastPrimaryTerm as BaseWpYoastPrimaryTerm;
use user\models\User;
use Yii;

/**
 * WpYoastPrimaryTerm model class for "wp_yoast_primary_term" database table
 *
 * Columns in table "wp_yoast_primary_term" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $id
 * @property integer $post_id
 * @property integer $term_id
 * @property string $taxonomy
 * @property string $created_at
 * @property datetime $updated_at
 * @property integer $blog_id
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpYoastPrimaryTerm extends BaseWpYoastPrimaryTerm
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['post_id, term_id, taxonomy, updated_at', 'required'],
			['post_id, term_id, blog_id', 'numerical', 'integerOnly' => true],
			['taxonomy', 'length', 'max'=> 32],
			['created_at, blog_id', 'default', 'setOnEmpty' => true, 'value' => null],
			['created_at', 'safe'],
			['id, post_id, term_id, taxonomy, created_at, updated_at, blog_id', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'post_id' => Yii::t('app', 'Post'),
			'term_id' => Yii::t('app', 'Term'),
			'taxonomy' => Yii::t('app', 'Taxonomy'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'blog_id' => Yii::t('app', 'Blog'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.post_id', $this->post_id);
        $criteria->compare('t.term_id', $this->term_id);
        $criteria->compare('t.taxonomy', $this->taxonomy, true);
        $criteria->compare('t.created_at', $this->created_at, true);
        $criteria->compare('t.updated_at', $this->updated_at);
        $criteria->compare('t.blog_id', $this->blog_id);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['id' => true]]
        ]);
    }


    /**
     * WpYoastPrimaryTerm models list
     * 
     * @return array
     */
    public function wpyoastprimaryterm_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['id', 'taxonomy'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WpYoastPrimaryTerm::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}
<?php
/**
 * @package dzlab\wordpress\models 
 */

namespace dzlab\wordpress\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\wordpress\helpers\WordpressHelper;
use dzlab\wordpress\models\WpPost as BaseWpPost;
use dzlab\wordpress\models\WpPostMeta;
use dzlab\wordpress\models\WpTermRelationship;
use dzlab\wordpress\models\WpUser;
use dzlab\wordpress\models\WpYoastIndexable;
use user\models\User;
use Yii;

/**
 * WpPost model class for "wp_posts" database table
 *
 * Columns in table "wp_posts" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $ID
 * @property integer $post_author
 * @property string $post_date
 * @property string $post_date_gmt
 * @property string $post_content
 * @property string $post_title
 * @property string $post_excerpt
 * @property string $post_status
 * @property string $comment_status
 * @property string $ping_status
 * @property string $post_password
 * @property string $post_name
 * @property string $to_ping
 * @property string $pinged
 * @property string $post_modified
 * @property string $post_modified_gmt
 * @property string $post_content_filtered
 * @property integer $post_parent
 * @property string $guid
 * @property integer $menu_order
 * @property string $post_type
 * @property string $post_mime_type
 * @property integer $comment_count
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class WpPostAttachment extends BaseWpPost
{
    /**
     * Special post type "attachment"
     */
    public $post_type = 'attachment';


    /**
     * Wordpress upload directory path
     */
    public $upload_path = 'assets/uploads/';


    /**
     * Array with size meta data information
     */
    private $_vec_sizes = [];


    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Check if the attachment is an image
     */
    public function is_image()
    {
        return preg_match("/^image\//", $this->post_mime_type);
    }


    /**
     * Get image sizes
     */
    public function get_sizes()
    {
        if ( empty($this->_vec_sizes) )
        {
            $vec_attachment_metadata = $this->get_field('_wp_attachment_metadata');
            if ( !empty($vec_attachment_metadata) && isset($vec_attachment_metadata['sizes']) )
            {
                $this->_vec_sizes = $vec_attachment_metadata['sizes'];
            }
        }

        return $this->_vec_sizes;
    }


    /**
     * Get an image sizes
     */
    public function get_size($size_name)
    {
        $vec_sizes = $this->get_sizes();
        if ( isset($vec_sizes[$size_name]) )
        {
            return $vec_sizes[$size_name];
        }

        return null;
    }


    /**
     * Check if image has a given size
     */
    public function has_size($size_name)
    {
        $vec_sizes = $this->get_sizes();
        return isset($vec_sizes[$size_name]);
    }
    

    /**
     * Get image URL
     */
    public function image_url($size_name = '')
    {
        $file_path = $this->get_field('_wp_attached_file');

        // URL for a preset image (special size)
        if ( !empty($size_name) )
        {
            $vec_size_info = $this->get_size($size_name);
            if ( $vec_size_info && isset($vec_size_info['file']) )
            {
                // Replace from "2020/11/IMG_4097.jpg" to "2020/11/IMG_4097-742x320.jpg"
                $vec_file_path = explode('/', $file_path);
                $vec_file_path[count($vec_file_path)-1] = $vec_size_info['file'];
                
                return Yii::app()->wordpressManager->site_url() . $this->upload_path . implode('/', $vec_file_path);
            }
        }

        return Yii::app()->wordpressManager->site_url() . $this->upload_path . $file_path;
    }
}
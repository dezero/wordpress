<?php
/**
 * Special functions to use on Wordpress
 * 
 * Usage instructions:
 * 
 *  1. Copy this file into your Wordpress project
 * 
 * 
 *  2. Replace DZ_BACKEND_URL with the correct URL of Yii backend
 * 
 * 
 *  3. Include this PHP file in your code
 * 
 *     // Example
 *     require('./<project_directory>/dz_functions.php');
 * 
 * 
 *  4. Use the function dz_backend_url($user_id) just for ADMIN users
 *     or users with access to Yii Backend
 * 
 *     // Example
 *      <a href="<?php echo dz_backend_url(); ?>" target="_blank">Go to backend</a>
 */

// REPLACE VALUE
defined('DZ_BACKEND_URL') or define('DZ_BACKEND_URL', "http://ffwi.dezero.es");


/**
 * Return the full URL to access to Yii backend directly from Wordpress
 * 
 * Example: http://ffwi.dezero.es/login-access?id=56&token=8ff0edc701abd6439663a9fc869780520895b2ddcc4d777f8a8b5318bf3f0f13
 */
function dz_backend_url($user_id = null)
{
    if ( $user_id === null )
    {
        $user_id = get_current_user_id();
    }

    $user_token = dz_get_token($user_id);
    return DZ_BACKEND_URL ."?login-access?id={$user_id}&token={$user_token}";
}


/**
 * Return a Wordpress token needed for accessing to Yii backend
 */
function dz_get_token($user_id = null)
{
    if ( $user_id === null )
    {
        $user_id = get_current_user_id();
    }

    // Get "session_tokens" meta_key for this user
    $user_meta = get_user_meta($user_id);
    if ( isset( $user_meta['session_tokens'] ) )
    {
        // Serialized value
        $session_tokens = maybe_unserialize( $user_meta['session_tokens'][0] );
        if ( !empty($session_tokens) && is_array($session_tokens) )
        {
            // Return last generated token
            $session_tokens = array_keys($session_tokens);
            $output_token = end($session_tokens);
            //reset($session_tokens);
            
            return $output_token;
        }
    }

    return null;
}

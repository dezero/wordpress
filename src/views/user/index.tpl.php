<?php
/*
|--------------------------------------------------------------------------
| Admin list page for WpUser models
|--------------------------------------------------------------------------
|
| Available variables:
|   $model: WpUser model class
|
*/  
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Wordpress Users Management');
?>

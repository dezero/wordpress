<?php
/*
|--------------------------------------------------------------------------
| Admin list page for WpPost models
|--------------------------------------------------------------------------
|
| Available variables:
|   $model: WpPost model class
|
*/  
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Wordpress Posts Management');
?>
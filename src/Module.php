<?php
/**
 * Module to manage Wordpress integration for DZ Framework
 */

namespace dzlab\wordpress;

use Yii;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        // Controller for WpPost models
        'post' => [
            'class' => 'dzlab\wordpress\controllers\PostController',
        ],

        // Controller for WpUser models
        'user' => [
            'class' => 'dzlab\wordpress\controllers\UserController',
        ],

        // Controller to login automatically from Wordpress admin
        'access' => [
            'class' => 'dzlab\wordpress\controllers\AccessController',
        ],
    ];


    /**
     * Default controller
     */
    public $defaultController = 'post';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['wordpress.css'];
    public $jsFiles = null; // ['wordpress.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        // Init this module with current path
        $this->init_module(__DIR__);

        // Going on with the init process
        parent::init();
    }
}
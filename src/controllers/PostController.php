<?php
/*
|--------------------------------------------------------------------------
| Controller class for WpPost models
|--------------------------------------------------------------------------
*/

namespace dzlab\wordpress\controllers;

use dz\helpers\Log;
use dz\web\Controller;
use dzlab\wordpress\models\WpPost;
use Yii;

class UserController extends Controller
{
    /**
     * List action for WpPost models
     */
    public function actionIndex()
    {
        $wp_post_model = Yii::createObject(WpPost::class, 'search');
        $wp_post_model->unsetAttributes();
        
        if ( isset($_GET['WpPost']) )
        {
            $wp_post_model->setAttributes($_GET['WpPost']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Render INDEX view
        $this->render('//wordpress/post/index', [
            'wp_post_model'     => $wp_post_model
        ]);
    }
}
<?php
/*
|--------------------------------------------------------------------------
| Controller class for WpUser models
|--------------------------------------------------------------------------
*/

namespace dzlab\wordpress\controllers;

use dz\helpers\Log;
use dz\web\Controller;
use dzlab\wordpress\models\WpUser;
use Yii;

class UserController extends Controller
{
    /**
     * List action for WpUser models
     */
    public function actionIndex()
    {
        $wp_user_model = Yii::createObject(WpUser::class, 'search');
        $wp_user_model->unsetAttributes();
        
        if ( isset($_GET['WpUser']) )
        {
            $wp_user_model->setAttributes($_GET['WpUser']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Render INDEX view
        $this->render('//wordpress/user/index', [
            'model'     => $wp_user_model
        ]);
    }
}

<?php
/*
|--------------------------------------------------------------------------
| Controller class to allow Wordpress admins to login automatically
|--------------------------------------------------------------------------
*/

namespace dzlab\wordpress\controllers;

use dz\helpers\Log;
use dz\web\Controller;
use dzlab\wordpress\models\WpUser;
use Yii;

class AccessController extends Controller
{
    /**
     * Redirect URL after login automatically
     */
    public $redirect_url = '/users/user';


    /**
     * Default action
     */
    public $defaultAction = 'publicIndex';


    /**
     * Main action
     */
    public function actionPublicIndex($token, $id)
    {
        // Check if WpUser model exists
        $wp_user_model = WpUser::findOne($id);
        if ( ! $wp_user_model )
        {
            Log::wp_login("Login error - Access denied - WpUser model does not exist (token = {$token} | wp_id = {$id})");
            throw new \CHttpException(401, Yii::t('app', 'Access denied'));
        }

        // Check given session token
        if ( ! $wp_user_model->check_token($token) )
        {
            Log::wp_login("Login error - Access denied - Token is invalid (token = {$token} | wp_id = {$id})");
            throw new \CHttpException(401, Yii::t('app', 'Access denied'));
        }

        // Check if user is admin on Wordpress
        if ( ! $this->check_role_access($wp_user_model) )
        {
            Log::wp_login("Login error - Access denied - Wordpress role's user is not allowed to access (token = {$token} | wp_id = {$id})");
            throw new CHttpException(401, Yii::t('app', 'Access denied'));  
        }

        // Check if User model exists or can be created on Dz Framework
        $user_model = Yii::app()->wordpressManager->dz_user_model($wp_user_model->ID, true);
        if ( ! $user_model )
        {
            Log::wp_login("Login error - Access denied - User model cannot be created on Dz Framework (token = {$token} | wp_id = {$id})");
            throw new \CHttpException(401, Yii::t('app', 'Access denied'));
        }

        // Get redirect URL
        $redirect_url = $this->redirect_url;
        if ( isset($_GET['redirect']) && !empty($_GET['redirect']) )
        {
            $redirect_url = $_GET['redirect'];
        }

        // User is already logged in? If so, logout and redirect here again
        if ( Yii::app()->user->id > 0 )
        {
            /*
            // Check if user has make logout before to avoid infinite redirects
            if ( isset($_GET['logout']) )
            {
                throw new CHttpException(401, Yii::t('app', 'Access denied'));          
            }
            Yii::app()->user->logout();
            $this->redirect([Yii::app()->request->url, 'logout' => 1]);
            */

            // Make the redirect
            $this->redirect($redirect_url);
        }
        else
        {
            // Make login automatically
            Yii::app()->userManager->auto_login($user_model->id);

            // Register login access in log files
            $ip_address = Yii::app()->request->getUserIP();
            Log::wp_login("User {$user_model->id} - {$user_model->username} ({$user_model->email}) logged in from IP {$ip_address} (token = {$token} | wp_id = {$id})");

            // After login process
            Yii::app()->userManager->afterLogin($user_model, $redirect_url);
        }
    }


    /**
     * Check if Wordpress roles can access to Wordpress backend
     */
    public function check_role_access($wp_user_model)
    {
        return true;
    }
}
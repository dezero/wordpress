<?php
/**
 * PasswordHelper class file for Dz Framework
 *
 * Helper class for working with Wordpress passwords
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2022 Fabián Ruiz
 */

namespace dzlab\wordpress\helpers;

use dzlab\wordpress\lib\PasswordHash;
use Yii;

class PasswordHelper
{
    /**
     * @var PasswordHash. PHPass object
     */
    private static $wp_hasher;


    /**
     * Create a hash (encrypt) of a plain text password.
     *
     * For integration with other applications, this function can be overwritten to
     * instead use the other package password checking algorithm.
     *
     * @since 2.5.0
     *
     * @global PasswordHash $wp_hasher PHPass object
     *
     * @param string $password Plain text user password to hash
     * @return string The hash string of the password
     */
    public static function wp_hash_password($password)
    {
        return self::wp_hasher()->HashPassword(trim($password));
    }


    /**
     * Checks the plaintext password against the encrypted Password.
     *
     * Maintains compatibility between old version and the new cookie authentication
     * protocol using PHPass library. The $hash parameter is the encrypted password
     * and the function compares the plain text password when encrypted similarly
     * against the already encrypted password to see if they match.
     *
     * For integration with other applications, this function can be overwritten to
     * instead use the other package password checking algorithm.
     *
     * @since 2.5.0
     *
     * @global PasswordHash $wp_hasher PHPass object used for checking the password
     *                                 against the $hash + $password
     * @uses PasswordHash::CheckPassword
     *
     * @param string     $password Plaintext user's password
     * @param string     $hash     Hash of the user's password to check against.
     * @return bool False, if the $password does not match the hashed password
     */
    public static function wp_check_password($password, $hash)
    {
        // If the hash is still md5...
        if ( strlen($hash) <= 32 )
        {
            if ( self::hash_equals($hash, md5($password)) )
            {
                $hash = self::wp_hash_password($password);
            }
        }

        // If the stored hash is longer than an MD5,
        // presume the new style phpass portable hash.
        return self::wp_hasher()->CheckPassword($password, $hash);
    }


    /**
     * Timing attack safe string comparison
     *
     * Compares two strings using the same time whether they're equal or not.
     *
     * Note: It can leak the length of a string when arguments of differing length are supplied.
     *
     * This function was added in PHP 5.6.
     * However, the Hash extension may be explicitly disabled on select servers.
     * As of PHP 7.4.0, the Hash extension is a core PHP extension and can no
     * longer be disabled.
     * I.e. when PHP 7.4.0 becomes the minimum requirement, this polyfill
     * can be safely removed.
     *
     * @since 3.9.2
     *
     * @param string $a Expected string.
     * @param string $b Actual, user supplied, string.
     * @return bool Whether strings are equal.
     */
    public static function hash_equals($a, $b)
    {
        $a_length = strlen($a);
        if ( strlen( $b ) !== $a_length )
        {
            return false;
        }

        $result = 0;

        // Do not attempt to "optimize" this.
        for ( $i = 0; $i < $a_length; $i++ )
        {
            $result |= ord($a[$i]) ^ ord($b[$i]);
        }

        return 0 === $result;
    }


    /**
     * Returns PasswordHash class, if it exists.
     */
    private static function wp_hasher()
    {
        if ( self::$wp_hasher === null )
        {
            self::$wp_hasher = new PasswordHash(8, true);
        }

        return self::$wp_hasher;
    }
}
